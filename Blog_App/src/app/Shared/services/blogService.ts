import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { postDto } from "../../_models/postDto";

const httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    )
  };


@Injectable({
    providedIn: 'root'
  })

export class BlogService{
/**
 *
 */
constructor(private http: HttpClient) {
  
    
}



baseUrl = 'https://localhost:7185/api/'


getAllPost(){
    debugger
    return this.http.get<postDto[]>(this.baseUrl+'post')
}

getPostById(id : number){
    debugger
    return this.http.get<postDto>(this.baseUrl+'post/'+ id)
}

saveBlogPost(model : postDto){
    return this.http.post(this.baseUrl+ 'post', model) 
}

updateBlogPost(model : postDto, id: number){
    return this.http.put(this.baseUrl+ 'post/' +id, model) 
}


deletePost(id : number){
    return this.http.delete(this.baseUrl+ 'post/'+ id)
}

}