export class postDto { 
    public postId: number; 
    public title : string;
    public content: string;
    public author : string;
    public dateCreated : Date;  
}