import { Component, OnInit } from '@angular/core';
import { postDto } from '../../../_models/postDto';

import { BlogService } from '../../../Shared/services/blogService';
import { ActivatedRoute, Router } from '@angular/router';
import { error } from 'console';


@Component({
  selector: 'app-EditBlog',
  templateUrl: './EditBlog.component.html',
  styleUrls: ['./EditBlog.component.css']
})
export class EditBlogComponent implements OnInit {

  postdto: postDto = new postDto();

  constructor(private route: ActivatedRoute, private router: Router, private blogService: BlogService) {

  }

  ngOnInit() {
    debugger
    this.route.queryParams.subscribe(params => {
      const id = params['id'];
      if (id > 0) {

        this.preaperToUpdate(id);
      }
    })
  }
 
  preaperToUpdate(id: number) {
    this.blogService.getPostById(id).subscribe((res: postDto) => {
      if (res != null || res != undefined) {
        this.postdto = res
      }
    })
  }

  updatepost() {
    this.blogService.updateBlogPost(this.postdto, this.postdto.postId).subscribe(
      (res: any) => {
        alert("Updated Successfully");
        this.router.navigate(["/"]);
      } 
    );
    
}
}
