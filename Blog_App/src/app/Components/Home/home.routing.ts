import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Home.component';
import { BlogComponent } from './Blog/Blog.component';
import { EditBlogComponent } from './EditBlog/EditBlog.component';
import { CreateBlogComponent } from './CreateBlog/CreateBlog.component';

const routes: Routes = [
  { path: '', component: HomeComponent,
  children: [
    {path : '' ,component: BlogComponent},
    {path: 'editblog', component : EditBlogComponent},
    {path: 'createblog', component: CreateBlogComponent}
  ]

},
];

export const HomeRoutes = RouterModule.forChild(routes);
