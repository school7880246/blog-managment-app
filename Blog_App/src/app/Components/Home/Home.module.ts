import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './Home.component';
import { HomeRoutes } from './home.routing';
import { BlogComponent } from './Blog/Blog.component';
import { EditBlogComponent } from './EditBlog/EditBlog.component';
import { CreateBlogComponent } from './CreateBlog/CreateBlog.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutes,
    FormsModule
  ],
  declarations: [
    HomeComponent,
    BlogComponent,
    EditBlogComponent,
    CreateBlogComponent
  ],

})
export class HomeModule { }
