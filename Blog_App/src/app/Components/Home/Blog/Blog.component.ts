import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../Shared/services/blogService';
import { postDto } from '../../../_models/postDto';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-Blog',
  templateUrl: './Blog.component.html',
  styleUrls: ['./Blog.component.css']
})
export class BlogComponent implements OnInit {

  postDTO: postDto [];


  constructor(private blogService : BlogService, private router : Router, private route : ActivatedRoute) { 

  }

  ngOnInit() {
    debugger
    this.getAllBlog();
  }


  getAllBlog() {
    this.blogService.getAllPost().subscribe(
      (res: postDto[]) => {
        debugger
        this.postDTO = res;
      },
      (error: HttpErrorResponse) => {
        console.error('Error fetching posts:', error); 
         alert('Failed to retrieve posts. Please try again later.')
      }
    );
  }

  deletePost(id : any){
    debugger
    this.blogService.deletePost(id).subscribe((res : any)=>{
    debugger 
      alert("post deleted!") 
      this.getAllBlog();
    })

  } 
 

  updatePost(id: number) {
    this.router.navigate(['/editblog'], {
      relativeTo: this.route,
      queryParams: {id : id} // Add your query parameters here
    });
  }
  

}
